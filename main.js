// Робота з базами даних
// Робота зі сторонніми API
// Робота з файлами та формами 
// Робота з мережевими запитами
const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];


function createBookList() {
  const bookList = document.createElement('ul');

  books.forEach((book) => {
    if (book.author && book.name && book.price) {
      const bookItem = document.createElement('li');
      const bookDetails = document.createTextNode(`${book.author}: ${book.name} - ${book.price} грн`);
      bookItem.appendChild(bookDetails);
      bookList.appendChild(bookItem);
    } else {
      console.error(`Помилка: невірний об'єкт - ${JSON.stringify(book)}`);
    }
  });

  const root = document.getElementById('root');
  root.appendChild(bookList);
}

createBookList();
createElement('ul') 





